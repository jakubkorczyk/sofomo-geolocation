module.exports = {
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
  ipApiToken: process.env.IP_API_TOKEN,
  databaseConnectionString: process.env.DATABSE_CONNECTION_STRING
}