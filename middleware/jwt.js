const createError = require('http-errors');
const jwt = require('jsonwebtoken');

function verifyAuthorization(req, res, next) {
  const token = req.headers['x-access-token'];
  if(!token) {
    return next(createError(403, 'No token provided.'))
  }

  jwt.verify(token, req.config.jwtSecret, (err, decoded) => {
    if(err) {
      return next(createError(500, 'Failed to authenticate token.'));
    }
    next();
  });
}

module.exports = verifyAuthorization;