const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.status(200).send({message: 'Api geolocation check based on ip.'});
});

module.exports = router;
