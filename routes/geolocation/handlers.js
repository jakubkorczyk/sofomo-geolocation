const createError = require('http-errors');

function getRealIp(req) {
  const ipAddr = req.headers["x-forwarded-for"];
  if (ipAddr){
    const list = ipAddr.split(",");
   return list[list.length-1];
  } else {
    return req.connection.remoteAddress;
  }
}

async function getGeolocation(ip, req, res) {
  const { database } = req.services;
  const geolocation = await database.get(ip)
  if(!geolocation) {
    return res.status(404).send( {message: `No geolocation stored for ip: ${ip}.`});
  }
  return res.status(200).send({ message: `Get geolocation for ip: ${ip}`, geolocation });
}

async function getByIp(req, res, next) {
  try {
    const ip = req.params.ip;
    await getGeolocation(ip, req, res);
  } catch (err) {
    next(createError(err.code || 500, err.message))
  }
}

async function getByRequestIp(req, res, next) {
  try {
    const ip = getRealIp(req);
    await getGeolocation(ip, req, res);
  } catch (err) {
    next(createError(err.code || 500, err.message, err.stack))
  }
}

async function saveGeolocation(ip, req, res) {
  const { ipService, database } = req.services;
  const geolocation =  await  ipService.getGeolocation(ip);

  if(geolocation) {
    database.add(ip, geolocation);
  }

  res.status(200).send({ message: `Geolocation for ${ip} stored succesfully.` });
}

async function postByIp(req, res, next) {
  try {
    const ip = req.params.ip;
    await saveGeolocation(ip, req, res)
  } catch (err) {
    next(createError(err.code || 500, err.message));
  }
}

async function postByRequestIp(req, res, next) {
  try {
    const ip = getRealIp(req);
    await saveGeolocation(ip, req, res);
  } catch (err) {
    next(createError(err.code || 500, err.message));
  }
}

async function deleteGeolocation(ip, req, res) {
  const { database } = req.services;

  await database.remove(ip);

  res.status(200).send({ message: `Geolocation for ${ip} removed.` });
}

async function deleteByIp(req, res, next) {
  try {
    const ip = req.params.ip;
    deleteGeolocation(ip, req, res);
  } catch (err) {
    next(createError(err.code || 500, err.message))
  }
}

async function deleteByRequestIp(req, res, next) {
  try {
    const ip = getRealIp(req);
    deleteGeolocation(ip, req, res);
  } catch (err) {
    next(createError(err.code || 500, err.message))
  }
}

module.exports = {
  getByIp,
  getByRequestIp,
  postByIp,
  postByRequestIp,
  deleteByIp,
  deleteByRequestIp
}