const express = require('express');

const handlers = require('./handlers');
const verifyAuthorization = require('../../middleware/jwt')

const router = express.Router();

router.use(verifyAuthorization)

router.get('/:ip', handlers.getByIp);
router.get('/', handlers.getByRequestIp);
router.post('/:ip', handlers.postByIp);
router.post('/', handlers.postByRequestIp);
router.delete('/:ip', handlers.deleteByIp);
router.delete('/p', handlers.deleteByRequestIp);

module.exports = router;
