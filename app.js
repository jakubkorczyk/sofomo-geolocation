const createError = require('http-errors');
const express = require('express');

const indexRouter = require('./routes/index');
const geolocationRouter = require('./routes/geolocation/routes');

const app = express();


function initilizeServer(database, ipService, config) {
  app.use((req, res, next) => {
    req.services = {
      database,
      ipService
    };
    req.config = config;
    next();
  });
  
  app.use('/', indexRouter);
  app.use('/geolocation', geolocationRouter);
  
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    return next(createError(404));
  });
  
  app.use((err, req, res, next) => {
    // render the error page
    console.log(`An error occured for: ${req.baseUrl}, err: ${err.message}, ${err.stack}`)
    res.status(err.status || 500);
    res.send({message: err.message});
  });

  return app
}



module.exports = initilizeServer;
