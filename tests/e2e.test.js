const supertest = require('supertest');

const initilizeServer = require('../app');

const ipService = {
  getGeolocation: jest.fn(( )=> ({ geolocationData: 'data'}))
};

const database = {
    add: jest.fn(),
    get: jest.fn((ip) => {
      console.log(ip)
      return {geolocationData: 'data'}
    }),
    remove: jest.fn()
};

const config = {
  jwtSecret: 'test'
}

const server = initilizeServer(database, ipService, config);
const request = supertest(server);

describe('Checks get responses for geolocation', () => {
  test('with not return data for not authoriezed users', () => {
    return request.get('/geolocation/')
    .expect(403);
  });
  test('with no ip sent will use ip from props', () => {
    return request.get('/geolocation')
    .set({
      'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.5mhBHqs5_DTLdINd9p5m7ZJ6XD0Xc55kIaCRY5r6HRA',
    })
    .expect(200)
    .expect(() => {
      expect(database.get).toBeCalledWith('::ffff:127.0.0.1');
    });
  });
  test('with no ip sent will use ip from request', () => {
    return request.get('/geolocation/198.111.111.1')
    .set({
      'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.5mhBHqs5_DTLdINd9p5m7ZJ6XD0Xc55kIaCRY5r6HRA',
    })
    .expect(200)
    .expect(() => {
      expect(database.get).toBeCalledWith('198.111.111.1');
    });
  });
});