const Redis = require('ioredis');
const createError = require('http-errors');

function connect(connectionString) {
  const redis = new Redis(connectionString);
  redis.on('connect', () => {
    console.log('Connected to Database');
  });
  redis.on('error', err => {
    console.log(`Error on Redis: ${err}`);
  });

  async function add(key, data) {
    try {
      await redis.set(key, JSON.stringify(data));
    } catch(err) {
      throw createError(500, err.message);
    }
  }

  async function get(key) {
    try {
      const data = await redis.get(key);
      if(!data) {
        return null;
      }
      return JSON.parse(data);
    } catch (err) {
      throw createError(500, err.message);
    }
  }

  async function remove(key) {
    try {
      await redis.del(key);
    } catch (err) {
      throw createError(500, err.message);
    }
  }

  return {
    add,
    get,
    remove
  }
}

module.exports = {
  connect
}
