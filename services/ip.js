const axios = require('axios');
const createError = require('http-errors');

const ipApiUrl = 'http://api.ipstack.com/';

function IpService(apiToken) {
  async function getGeolocation(ip) {  
    const response = await axios.get(`${ipApiUrl}/${ip}?access_key=${apiToken}`);
    if(response.data.error) {
      const { info, code } = response.data.error;
      throw createError(code, info);
    }
    return response.data;
  }

  return {
    getGeolocation
  };
}

module.exports = IpService;