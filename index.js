require('dotenv').config();

const database = require('./services/database');
const IpService = require('./services/ip');

const config = require('./config/config');
const initilizeServer = require('./app');

const port = config.port || 3000;

(async () => {
  try {
    const dbConnection = database.connect(config.databaseConnectionString);
    const ipService = IpService(config.ipApiToken);
    const server = initilizeServer(dbConnection, ipService, config);
    
    server.listen(port, (err) => {
      if(err) {
        console.log(`Coudn't start the server: ${err.message}.`);
      }
      console.log(`App is listining on port: ${port}`);
    })
  } catch(err) {
    console.log(`Coudn't start the server: ${err.message}.`);
  }
})();


